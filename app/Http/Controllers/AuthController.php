<?php

namespace App\Http\Controllers;

use Str;
use Mail;
use App\Models\User;
use App\Mail\RegisterMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function index(){
        return view('auth.login');
    }

    public function login(Request $request){

        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);
 
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
 
            return redirect('/dashboard');
        }
 
        return back()->withErrors([
            'email' => 'Email ou senha errado(s)',
        ])->onlyInput('email');

    }
    
    public function register(){
        return view('auth.register');

    }

    public function verify($token){

        $user = User::where('remember_token','=',$token)->first();
        
        if(!empty($user)){
            $user->email_verified_at = date('Y-m-d H:i:s');
            $user->remember_token = Str::random(40);
            $user->save();
            return redirect('/')->with('success',"Seu email foi verificado com sucesso.");
        }else{
            abort(404);
        }

    }
    public function forgot_password(){
        return view('auth.forgot_password');

    }

    public function register_user(Request $request){        

        request()->validate([
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required'
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->remember_token = Str::random(40);
        $user->save();

        if($user){
            //Mail::to($user->email)->send(new RegisterMail($user));
            return redirect('/')->with('success',"Registro realizado com sucesso. Por favor, verifique seu email");
        }
           
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }
}
