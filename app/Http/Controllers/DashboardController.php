<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{User,Empresa};
class DashboardController extends Controller
{
    public function index(){

        $qtd_usuarios = User::count();
        $qtd_empresas = Empresa::count();

        return view('backend.dashboard',compact('qtd_usuarios','qtd_empresas'));
    }
}
