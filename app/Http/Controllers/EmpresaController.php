<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EmpresaRequest;
use App\Models\Empresa;
use App\Models\Cidade;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresas = Empresa::with('cidade')->paginate(10);
        return view('empresa.index',compact('empresas'));
    }

    public function create(){

        $cidades = Cidade::with('estado')->get();
        return view('empresa.create',compact('cidades'));
    }
    
    public function edit($id){

        $empresa = Empresa::find($id);
        $cidades = Cidade::with('estado')->get();
        if($empresa){
            return view('empresa.edit',compact('empresa','cidades'));
        }        
    }
    
    public function store(EmpresaRequest $request)
    {   
        $data = $request->except('_token');
        $empresa = Empresa::create($data);
        return redirect('/empresas')->with('success',"Empresa cadastrada com sucesso");
    }

    // Display the specified Empresa.
    public function show($id)
    {
        $empresa = Empresa::find($id);

        if (!$empresa) {
            return response()->json('Empresa not found', 404);
        }

        return redirect('/empresas');
    }

    public function update(EmpresaRequest $request, $id)
    {
        $empresa = Empresa::find($id);
    
        if (!$empresa) {
            return response()->json('Empresa not found', 404);
        }
    
        $empresa->update($request->all());
        return redirect('/empresas')->with('success',"Empresa atualizada com sucesso");
    }
    public function destroy($id)
    {
        $empresa = Empresa::find($id);

        if (!$empresa) {
            return response()->json('Empresa not found', 404);
        }

        $empresa->delete();
        return redirect('/empresas')->with('success',"Empresa excluída com sucesso");
    }

}
