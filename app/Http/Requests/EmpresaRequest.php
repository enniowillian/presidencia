<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'cnpj' => 'required|cnpj|max:20|unique:empresas,cnpj',
            'razao_social' => 'required|max:255',
            'nome_fantasia' => 'required|max:255',
            'cep' => 'required|max:20',
            'logradouro' => 'required|max:255',
            'bairro' => 'required|max:255',
            'complemento' => 'max:255',
            'telefone' => 'required|max:20',
            'email' => 'required|email|max:255',
            'cidade_id' => 'required',
        ];

        if ($this->method() === 'put' || $this->method === 'PUT') {
            
            $rules['cnpj'] = [
                'required',
                'max:20',
                \Illuminate\Validation\Rule::unique('empresas')->ignore($this->id)
            ]; 
        }

        return $rules;
    }
}
