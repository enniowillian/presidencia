<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [AuthController::class,'index']);
Route::post('/', [AuthController::class,'login'])->name('login');
Route::get('/register', [AuthController::class,'register']);
Route::get('/verify/{token}', [AuthController::class,'verify']);
Route::post('/register', [AuthController::class,'register_user'])->name('register_user');
Route::get('/forgot_password', [AuthController::class,'forgot_password']);
Route::post('/logout',[AuthController::class,'logout'])->name('logout');


Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [DashboardController::class,'index']);
    Route::get('/empresas',[EmpresaController::class,'index']);
    Route::get('/empresas/create',[EmpresaController::class,'create']);
    Route::post('/empresas',[EmpresaController::class,'store']);
    Route::get('/empresas/{id}/edit',[EmpresaController::class,'edit']);
    Route::put('/empresas/{id}/update',[EmpresaController::class,'update']);
    Route::delete('/empresas/{id}',[EmpresaController::class,'destroy']);

});