<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->id();
            $table->string('cnpj',20)->unique();
            $table->string('razao_social',255);
            $table->string('nome_fantasia',255);
            $table->string('cep',20);
            $table->string('logradouro',255);
            $table->string('bairro',255);
            $table->string('complemento',255)->nullable();            
            $table->string('telefone',20);
            $table->string('email',255);
            $table->unsignedBigInteger('cidade_id'); 
            $table->foreign('cidade_id')->references('id')->on('cidades');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('empresas');
    }
};
