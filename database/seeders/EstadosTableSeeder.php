<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $estados = [
            ['nome' => 'Distrito Federal', 'uf' => 'DF'],
            ['nome' => 'Goiás', 'uf' => 'GO'],
            // Adicione mais estados conforme necessário
        ];
    
        foreach ($estados as $estado) {
            DB::table('estados')->insert($estado);
        }
    }
}
