<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $cidades = [
            ['nome' => 'Brasília', 'estado_id' => 1],
            ['nome' => 'Goiânia', 'estado_id' => 2],
            // Adicione mais cidades conforme necessário
        ];
    
        foreach ($cidades as $cidade) {
            DB::table('cidades')->insert($cidade);
        }
    }
}
