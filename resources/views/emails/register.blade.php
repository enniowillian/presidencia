@component('mail::message')

<p>Olá, {{$user->name}}. </p>
   
Precisamos que você verifique seu email. Para verificar, basta clicar no botão abaixo:

@component('mail::button',['url'=> url('verify/' . $user->remember_token)])
Verificar
@endcomponent

<p>Obrigado</p>

{{config('app.name')}}

@endcomponent