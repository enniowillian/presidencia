<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Cadastro</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{url('assets/img/favicon.png')}}" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{url('assets/vendor/bootstrap/css/bootstrap.min.css')}} " rel="stylesheet">
  <link href="{{url('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{url('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{url('assets/vendor/quill/quill.snow.css')}}" rel="stylesheet">
  <link href="{{url('assets/vendor/quill/quill.bubble.css')}}" rel="stylesheet">
  <link href="{{url('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
  <link href="{{url('assets/vendor/simple-datatables/style.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{url('assets/css/style.css')}}" rel="stylesheet">
</head>

<body>

@include('layouts._header')

@include('layouts._sidebar')

  <main id="main" class="main">

    <div class="pagetitle">
      <h1>Cadastro de Empresas</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Empresas</a></li>
          <li class="breadcrumb-item active">Cadastro</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
      <div class="row">
        <!-- Right side columns -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                  <h5 class="card-title">Cadastro de Empresa</h5>
                  @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>    
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="row g-3" method="post" action="/empresas">
                      @csrf
                      <div class="row">
                          <div class="col-4">
                              <label for="cnpj" class="form-label">CNPJ</label>
                              <input type="text" class="form-control" id="cnpj" name="cnpj" value="{{old('cnpj')}}">
                          </div>
                          <div class="col-8">
                              <label for="razao_social" class="form-label">Razão Social</label>
                              <input type="text" class="form-control" id="razao_social" name="razao_social" value="{{old('razao_social')}}">
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-12">
                              <label for="nome_fantasia" class="form-label">Nome Fantasia</label>
                              <input type="text" class="form-control" id="nome_fantasia" name="nome_fantasia" value="{{old('nome_fantasia')}}">
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="col-4">
                              <label for="cep" class="form-label">CEP</label>
                              <input type="text" class="form-control" id="cep" name="cep" value="{{old('cep')}}">
                          </div>
                          <div class="col-8">
                              <label for="logradouro" class="form-label">Logradouro</label>
                              <input type="text" class="form-control" id="logradouro" name="logradouro" value="{{old('logradouro')}}">
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-4">
                              <label for="bairro" class="form-label">Bairro</label>
                              <input type="text" class="form-control" id="bairro" name="bairro" value="{{old('bairro')}}">
                          </div>
                          <div class="col-4">
                              <label for="complemento" class="form-label">Complemento</label>
                              <input type="text" class="form-control" id="complemento" name="complemento" value="{{old('complemento')}}">
                          </div>
                          <div class="col-4">
                            <label for="cidade_id" class="form-label">Cidade</label>
                            <select id="cidade_id" class="form-select" name="cidade_id">
                              <option selected>Escolha uma cidade</option>
                              @foreach ($cidades as $cidade)
                                  <option value="{{ $cidade->id }}">{{ $cidade->nome }}</option>
                              @endforeach
                            </select>
                        </div>
                        
                      </div>
                      
                      <div class="row">
                          <div class="col-12">
                              <label for="telefone" class="form-label">Telefone</label>
                              <input type="text" class="form-control" id="telefone" name="telefone" value="{{old('telefone')}}">
                          </div>
                          <div class="col-12">
                              <label for="email" class="form-label">Email</label>
                              <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}">
                          </div>
                      </div>                    
                      
                      <div class="text-center">
                          <button type="submit" class="btn btn-primary">Salvar</button>
                          <button type="reset" class="btn btn-secondary">Limpar</button>
                          <a class="btn btn-info" href="{{url('/empresas')}}">Voltar</a>
                      </div>
                  </form><!-- End Vertical Form -->
              
                </div>
              </div>
        </div><!-- End Right side columns -->

      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
@include('layouts._footer')

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{url('assets/vendor/apexcharts/apexcharts.min.js')}}"></script>
  <script src="{{url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{url('assets/vendor/chart.js/chart.umd.js')}}"></script>
  <script src="{{url('assets/vendor/echarts/echarts.min.js')}}"></script>
  <script src="{{url('assets/vendor/quill/quill.min.js')}}"></script>
  <script src="{{url('assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
  <script src="{{url('assets/vendor/tinymce/tinymce.min.js')}}"></script>
  <script src="{{url('assets/vendor/php-email-form/validate.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{url('assets/js/main.js')}}"></script>

  <!-- Incluindo jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- Incluindo jQuery Mask Plugin -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>


<script>
  $(document).ready(function(){
      $('#telefone').mask('(00) 0000-00009');
      $('#telefone').blur(function(event) {
          if($(this).val().length == 15) { // Celular com 9 dígitos
              $('#telefone').mask('(00) 00000-0009');
          } else {
              $('#telefone').mask('(00) 0000-00009');
          }
      });
      $('#cnpj').mask('00.000.000/0000-00');
      $('#cep').mask('00000-000');
  });
  </script>

</body>

</html>